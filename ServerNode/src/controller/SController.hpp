#ifndef SERVER_CONTROLLER_H
#define SERVER_CONTROLLER_H

#include "../dto/type0_DTO.hpp"
#include "../dto/type2_DTO.hpp"
#include "../dto/lastestData_DTO.hpp"
#include "../dto/lastTimeData_DTO.hpp"
#include "../DataControl.hpp"
#include <oatpp/core/macro/codegen.hpp>
#include <oatpp/core/macro/component.hpp>
#include <oatpp/web/server/api/ApiController.hpp>

#include OATPP_CODEGEN_BEGIN(ApiController)

class SController : public oatpp::web::server::api::ApiController
{
    private:
        DataControl dataCont;
    public:
        SController(OATPP_COMPONENT(std::shared_ptr<ObjectMapper>,objectMapper))
        : oatpp::web::server::api::ApiController(objectMapper) {}

        ENDPOINT("POST","DeviceUploadData/type0",Device0,BODY_DTO(Object<UploadDTO0>,deviceType0))
        {
            //OATPP_LOGD("Device0","Receive Data: ID=%d , Value=%d",*(deviceType0->DeviceID),*(deviceType0->Value));
            dataCont.addType0Data(deviceType0->DeviceID,deviceType0->Value);
            return createResponse(Status::CODE_200,"Success");
        }

        ENDPOINT("POST","DeviceUploadData/type2",Device2,BODY_DTO(Object<UploadDTO2>,deviceType2))
        {
            dataCont.addType2Data(deviceType2->DeviceID,deviceType2->CPU,deviceType2->MEM);
            return createResponse(Status::CODE_200,"Success");
        }

        //Test: curl -X POST -d '{ "DeviceID" : 0 }' http://127.0.0.1:8510/LastestData
        ENDPOINT("POST","LastestData",lastestData,BODY_DTO(Object<RequestLastestData>,ldata))  //JSON解析
        {
            std::string str=dataCont.getLastTimeValue(ldata->DeviceID);
            if(str.size()>0)
                return createResponse(Status::CODE_200,str);
            else
                return createResponse(Status::CODE_202,"No Data");
        }

        //Test: curl -X POST http://127.0.0.1:8510/DeviceList
        ENDPOINT("POST","DeviceList",deviceList)
        {
            return createResponse(Status::CODE_200,dataCont.getDeviceList());
        }
        
        //Test: curl -X POST -d '{ "DeviceID" : 0 , "timeLength" : 1}' http://127.0.0.1:8510/HistoryData
        ENDPOINT("POST","HistoryData",historyData,BODY_DTO(Object<LastTimeData_DTO>,ldata))
        {
            return createResponse(Status::CODE_200,dataCont.getHistoryData(ldata->DeviceID,ldata->timeLength));
        }

        //curl -X POST http://127.0.0.1:8510/SystemInfo
        ENDPOINT("POST","SystemInfo",systemInfo)
        {
            return createResponse(Status::CODE_200,dataCont.getServerInfo());
        }

        //curl -X POST http://127.0.0.1:8510/SystemStatus
        ENDPOINT("POST","SystemStatus",systemStatus)
        {
            return createResponse(Status::CODE_200,dataCont.getServerStatus());
        }
};

#include OATPP_CODEGEN_END(ApiController)

#endif