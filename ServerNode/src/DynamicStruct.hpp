#ifndef DYNAMIC_STRUCT_H
#define DYNAMIC_STRUCT_H

#include <string>
#include <map>
#include <list>
#include <nlohmann/json.hpp>
#include <initializer_list>

//这个实现有一个缺陷，无法保证key唯一

class DynamicStruct
{
    public:
        DynamicStruct();
        DynamicStruct(std::initializer_list<std::pair<std::string,double>> initList);  //仅支持数字进行列表初始化
        ~DynamicStruct();
        void cloneNum(const DynamicStruct& scoreObject);  //复制scoreObject的数字键，但不复制值，所有值为0
        void addKey(std::string Key, double Value);
        void addKey(std::string Key, std::string Value);
        bool getKey(std::string Key, double& Value);
        bool getKey(std::string Key, std::string& Value);
        bool setKey(std::string Key, const double Value);
        bool setKey(std::string Key, const std::string& Value);
        bool addNum(const DynamicStruct& scoreObject);
        void divisionNum(double Value);
        std::list<std::string> getAllNumKey();
        bool removeKey(std::string Key);
        nlohmann::json toJson();
    private:
        std::map<std::string,double> numMap;
        std::map<std::string,std::string> strMap;
};

#endif