#ifndef SYSTEM_INFO_H
#define SYSTEM_INFO_H

#include <sys/utsname.h>
#include <sys/statfs.h>
#include <sys/unistd.h>
#include <cstring>
#include <string>
#include <fstream>
#include <oatpp/core/macro/component.hpp>

class SystemInfo
{
    public:
        double getCPUusage();
        double getMemoryUsage();
};

#endif
