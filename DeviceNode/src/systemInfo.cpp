#include "systemInfo.hpp"

struct CPUInfo
{
    double user;  
    double nice;
    double system;
    double idle;
    double iowait;
    double irq;
    double softirq;
    double steal;
    double guest;
    double guest_nice;
};


double SystemInfo::getCPUusage()
{
    std::ifstream iFile("/proc/stat");
    if(!iFile.is_open())
    {
        OATPP_LOGE("SystemInfo","Can't open file : /proc/stat");
        return 100;
    }
    std::string getItem;
    iFile>>getItem;
    if(getItem!="cpu")
    {
        OATPP_LOGE("SystemInfo","At /proc/stat, Fist line value not 'cpu' ");
        return 100;
    }
    CPUInfo cpuInfo;
    iFile>>cpuInfo.user;
    iFile>>cpuInfo.nice;
    iFile>>cpuInfo.system;
    iFile>>cpuInfo.idle;
    iFile>>cpuInfo.iowait;
    iFile>>cpuInfo.irq;
    iFile>>cpuInfo.softirq;
    iFile>>cpuInfo.steal;
    iFile>>cpuInfo.guest;
    iFile>>cpuInfo.guest_nice;
    if(!iFile.good())
    {
        OATPP_LOGE("SystemInfo","At /proc/stat, Read file not good ");
        return 100;
    }
    iFile.close();
    //Average idle time (%) = (idle * 100) / (user + nice + system + idle + iowait + irq + softirq + steal + guest + guest_nice)
    return 100-(cpuInfo.idle*100)/(cpuInfo.user + cpuInfo.nice + cpuInfo.system + cpuInfo.idle + cpuInfo.iowait + cpuInfo.irq + cpuInfo.softirq + cpuInfo.steal + cpuInfo.guest + cpuInfo.guest_nice);
}

struct MemoryInfo
{
    double MemTotal;
    double MenFree;
};


double SystemInfo::getMemoryUsage()
{
    std::ifstream iFile("/proc/meminfo");
    if(!iFile.is_open())
    {
        OATPP_LOGE("SystemInfo","Can't open file : /proc/meminfo");
        return 100;
    }
    std::string itemTitle;
    iFile>>itemTitle;
    if(itemTitle!="MemTotal:")
    {
        OATPP_LOGE("SystemInfo","At /proc/memory, Fist line value not 'MemTotal:' ");
        return 100;       
    }
    MemoryInfo memoryInfo;
    iFile>>memoryInfo.MemTotal;

    iFile>>itemTitle;
    iFile>>itemTitle;
    iFile>>itemTitle;

    iFile>>itemTitle;
    iFile>>itemTitle;
    if(itemTitle!="MemAvailable:")
    {
        OATPP_LOGE("SystemInfo","At /proc/memory, Fist line value not 'MemAvailable:' ");
        return 100;       
    }
    iFile>>memoryInfo.MenFree;
    if(!iFile.good())
    {
        OATPP_LOGE("SystemInfo","At /proc/memory, Read file not good ");
        return 100;
    }

    iFile.close();
    return 100-(memoryInfo.MenFree/memoryInfo.MemTotal*100);
}
