#ifndef LAST_TIME_DATA_H
#define LAST_TIME_DATA_H

#include <oatpp/core/Types.hpp>
#include <oatpp/core/macro/codegen.hpp>

#include OATPP_CODEGEN_BEGIN(DTO)

class LastTimeData_DTO : oatpp::DTO
{
    DTO_INIT(LastTimeData_DTO,oatpp::DTO)
    DTO_FIELD(Int32,DeviceID);
    DTO_FIELD(Int32,timeLength);
};

#include OATPP_CODEGEN_END(DTO)

#endif