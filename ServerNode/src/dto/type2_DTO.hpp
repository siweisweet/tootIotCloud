#ifndef TYPE2_DTO_H
#define TYPE2_DTO_H

#include <oatpp/core/Types.hpp>
#include <oatpp/core/macro/codegen.hpp>

#include OATPP_CODEGEN_BEGIN(DTO)

class UploadDTO2 : public oatpp::DTO
{
    DTO_INIT(UploadDTO2,oatpp::DTO)
    DTO_FIELD(Int32,DeviceID);
    DTO_FIELD(Float32,CPU);
    DTO_FIELD(Float32,MEM);
};

#include OATPP_CODEGEN_END(DTO)

#endif