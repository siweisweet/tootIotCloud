#建立临时表
CREATE TABLE IF NOT EXISTS dateCurrent (
    ID BIGINT AUTO_INCREMENT,
    TypeID BIGINT,
    DeviceID BIGINT,
    FieldName VARCHAR(10),
    Value DOUBLE,
    PRIMARY KEY(ID)
);

DELIMITER //
#定义过程名
CREATE PROCEDURE funcDailyData()
    BEGIN
        #定义循环结束标志 
        DECLARE done INT DEFAULT FALSE;
        #定义字段变量
        DECLARE cTypeID BIGINT;
        DECLARE cTypeField VARCHAR(50);
        DECLARE jsonSearch VARCHAR(52);
        #定义游标
        DECLARE cur CURSOR FOR SELECT TypeID,TypeField FROM tootIoT.TypeFieldIndex;
        #是否找到数据,TRUE离开循环
        DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
        #打开游标
        OPEN cur;
        #清空表
        TRUNCATE dateCurrent;
        #循环读取
        read_loop: LOOP
            #获取当前游标数据
            FETCH cur INTO cTypeID,cTypeField;
            SET jsonSearch = CONCAT('$.',cTypeField);
            #判断是否结束
            IF done THEN
                LEAVE read_loop;
            END IF;
            #插入数据
            INSERT INTO tootIoT.dateCurrent (TypeID,DeviceID,FieldName,Value)
                SELECT 
                    AllDeviceInfo.DeviceType,
                    DataCollect.DeviceID,
                    cast( cTypeField as VARCHAR(10)) as ItemType, 
                    AVG(JSON_VALUE(Value,jsonSearch)) as Value 
                FROM DataCollect
                INNER JOIN AllDeviceInfo ON AllDeviceInfo.DeviceID=DataCollect.DeviceID
                WHERE DATE(uploadTime)=DATE(DATE_SUB(NOW(),INTERVAL 1 DAY)) AND AllDeviceInfo.DeviceType=cTypeID
                GROUP BY DataCollect.DeviceID;
        END LOOP;
        #关闭游标
        CLOSE cur;
        #插入数据
        INSERT INTO DataDayCollect SELECT DATE(DATE_SUB(NOW(),INTERVAL 1 DAY)), DeviceID, JSON_OBJECTAGG(FieldName,Value) FROM dateCurrent GROUP BY DeviceID;
    END;
DELIMITER ;

#定义事件
CREATE EVENT dailyDataCollection ON SCHEDULE EVERY 1 DAY DO CALL funcDailyData;