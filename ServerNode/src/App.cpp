#include "controller/SController.hpp"
#include "AppComponent.hpp"
#include <oatpp/network/Server.hpp>

#include <thread>

void run()
{
    AppComponent components;
    OATPP_COMPONENT(std::shared_ptr<oatpp::web::server::HttpRouter>,router);
    router->addController(std::make_shared<SController>());
    OATPP_COMPONENT(std::shared_ptr<oatpp::network::ConnectionHandler>,connectionHandler);
    OATPP_COMPONENT(std::shared_ptr<oatpp::network::ServerConnectionProvider>,connectionProvider);
    oatpp::network::Server server(connectionProvider,connectionHandler);
    OATPP_LOGI("tootIoTServer","Server running on port %s",connectionProvider->getProperty("port").getData());
    server.run();
}

int main()
{
    oatpp::base::Environment::init();

    std::thread serverThread(run);
    serverThread.join();

    oatpp::base::Environment::destroy();
    return 0;
}