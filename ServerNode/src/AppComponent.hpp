#ifndef APP_COMPONENT_H
#define APP_COMPONENT_H

#include <oatpp/web/server/HttpConnectionHandler.hpp>
#include <oatpp/network/tcp/server/ConnectionProvider.hpp>
#include <oatpp/parser/json/mapping/ObjectMapper.hpp>
#include <oatpp/core/macro/component.hpp>

#include "IoTServerConfig.hpp"

class AppComponent
{
    public:
        OATPP_CREATE_COMPONENT(std::shared_ptr<oatpp::network::ServerConnectionProvider>,serverConnectionProvider)
        ([]{
            return oatpp::network::tcp::server::ConnectionProvider::createShared
            ({ServerPath,ServerPort,oatpp::network::Address::IP_4});
        }());

        OATPP_CREATE_COMPONENT(std::shared_ptr<oatpp::web::server::HttpRouter>,httpRouter)
        ([] {
            return oatpp::web::server::HttpRouter::createShared();
        }());

        OATPP_CREATE_COMPONENT(std::shared_ptr<oatpp::network::ConnectionHandler>,serverConnectionHandler)
        ([]{
            OATPP_COMPONENT(std::shared_ptr<oatpp::web::server::HttpRouter>,router);
            return oatpp::web::server::HttpConnectionHandler::createShared(router);
        }());

        OATPP_CREATE_COMPONENT(std::shared_ptr<oatpp::data::mapping::ObjectMapper>,apiObjectMapper)
        ([]{
            return oatpp::parser::json::mapping::ObjectMapper::createShared();
        }());
};

#endif