#ifndef DATABASE_CONTROL_H
#define DATABASE_CONTROL_H

#include <mariadb/conncpp.hpp>
#include <fmt/format.h>
#include <oatpp/core/macro/component.hpp>
#include <nlohmann/json.hpp>
#include <list>
#include <utility>
#include "DynamicStruct.hpp"
#include "Time.hpp"

struct DeviceTypeItem
{
    std::string typeName;
    std::vector<std::string> typeField;
};

struct DeviceIDItem
{
    std::string deviceName;
    int32_t deviceType;
};

class DatabaseControl
{
    public:
        DatabaseControl();
        ~DatabaseControl();
        void uploadDeviceValue(int32_t deviceID,std::string value);  //参数value一定要是json格式
        std::list<std::pair<int32_t,DeviceIDItem>> downloadDeviceInfo();
        std::list<std::pair<int32_t,DeviceTypeItem>> downloadDeviceType();
        std::array<std::string,60> downloadDeviceLast1Hour(int32_t deviceID);
        std::array<DynamicStruct,24> downloadDeviceLast12Hour(int32_t deviceID,std::vector<std::string> typeField);  //30min
        std::array<DynamicStruct,24> downloadDeviceLast24Hour(int32_t deviceID,std::vector<std::string> typeField);  //1H
        std::array<DynamicStruct,7> downloadDeviceLast7Day(int32_t deviceID,std::vector<std::string> typeField); //7D
    private:
        void ReconnectDB();
        std::unique_ptr<sql::Connection> conn;
        bool checkDBconn();
};

#endif