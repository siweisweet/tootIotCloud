#ifndef DATA_TRANSMIT_H
#define DATA_TRANSMIT_H

#include "DeviceClient.hpp"
#include "DeviceInfo.h"

class DataTransmit
{
    private:
        constexpr static const char* TAG="DataTransmit";
    public:
        void static UploadData(const std::shared_ptr<DeviceClient>& client,float valueCPU,float valueMEM)
        {
            auto dto=UploadDTO::createShared();
            dto->DeviceID=Device_Id;
            dto->CPU=valueCPU;
            dto->MEM=valueMEM;
            try
            {
                oatpp::String data=client->uploadPost(dto)->readBodyToString();
                //OATPP_LOGD(TAG,"transmit data %s",data->c_str());
            }
            catch(const std::exception& e)
            {
                OATPP_LOGE(TAG,"Can't Upload Data : %s",e.what());
                return;
            }
        }
};

#endif