#ifndef SYSTEM_INFO_H
#define SYSTEM_INFO_H

#include <sys/utsname.h>
#include <sys/statfs.h>
#include <sys/unistd.h>
#include <cstring>
#include <string>
#include <fstream>
#include <oatpp/core/macro/component.hpp>

class SystemInfo
{
    public:
        std::string getSystemName();
        std::string getSystemVersion();
        double getCPUusage();
        double getMemoryUsage();
        double getDiskUsage(); //程序所在路径
        long getUptime();  //系统已经运行时间(秒)
    private:
        //这里定义一些只读信息，加快程序速度
        std::string SystemName;
        std::string SystemVersion;
};

#endif
