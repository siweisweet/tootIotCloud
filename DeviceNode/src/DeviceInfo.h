/*
DeviceInfo.h
用于配置设备类型和唯一ID
*/

#ifndef DEVICE_INFO_H
#define DEVICE_INFO_H

static const int Device_Type = 2;
static const int Device_Id = 3;
static const char* ServerPath = "127.0.0.1";
static const int ServerPort = 8510;

#endif