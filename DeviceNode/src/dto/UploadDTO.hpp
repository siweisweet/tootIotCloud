#ifndef UPLOAD_DTO_H
#define UPLOAD_DTO_H

#include <oatpp/core/Types.hpp>
#include <oatpp/core/macro/codegen.hpp>

#include OATPP_CODEGEN_BEGIN(DTO)

class UploadDTO : public oatpp::DTO
{
    DTO_INIT(UploadDTO,oatpp::DTO)
    DTO_FIELD(Int32,DeviceID);
    DTO_FIELD(Int32,CPU);
    DTO_FIELD(Int32,MEM);
};

#include OATPP_CODEGEN_END(DTO)

#endif