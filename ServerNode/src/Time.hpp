#ifndef TIME_H
#define TIME_H

#include <string>
class Time
{
    public:
        Time()=default;
        Time(std::string str)  //hh:mm::ss
        {
            int findPos=0;
            int lastPos=0;
            findPos=str.find(':',0);
            Hour=std::stoi(str.substr(0,findPos));
            lastPos=findPos+1;
            findPos=str.find(':',lastPos);
            Minute=std::stoi(str.substr(lastPos,findPos-lastPos));
            lastPos=findPos+1;
            Seconds=std::stoi(str.substr(lastPos,str.length()-1-lastPos));
        }
        operator long() const
        {
            return Hour*3600+Minute*60+Seconds;
        }
    private:
        int Hour;
        int Minute;
        int Seconds;
};

#endif