#ifndef TIMER_H
#define TIMER_H

#include <thread>
#include <functional>
#include <atomic>
/*
这是一个简易定时器，这是有缺陷的
如果定时时间很长，可能需要很长时间才能析构或者停止
*/

class SimpleTimer
{
    public:
        SimpleTimer();
        ~SimpleTimer();
        void SetTime(std::chrono::seconds sec);
        void Start();
        void Stop();
        void SetTask(std::function<void()> task);
    private:
        std::function<void()> timerTask;   //定时器任务
        std::chrono::seconds timerSec;
        std::thread* timerThread;
        std::atomic<bool> timerStatus;
        class Runnable   //定时器执行体
        {
            public:
                Runnable(SimpleTimer* _controlObject);
                Runnable& operator() ();
            private:
                SimpleTimer* controlObject;
        };
};

#endif