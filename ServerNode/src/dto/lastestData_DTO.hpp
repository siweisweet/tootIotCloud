#ifndef LASTEST_DATA_DTO_H
#define LASTEST_DATA_DTO_H

#include <oatpp/core/Types.hpp>
#include <oatpp/core/macro/codegen.hpp>

#include OATPP_CODEGEN_BEGIN(DTO)

class RequestLastestData : public oatpp::DTO
{
    DTO_INIT(RequestLastestData,oatpp::DTO)
    DTO_FIELD(Int32,DeviceID);
};

#include OATPP_CODEGEN_END(DTO)

#endif