#ifndef DEVICE_CLIENT_H
#define DEVICE_CLIENT_H

#include "dto/UploadDTO.hpp"
#include <oatpp/web/client/ApiClient.hpp>
#include <oatpp/core/macro/codegen.hpp>

class DeviceClient : public oatpp::web::client::ApiClient
{
    #include OATPP_CODEGEN_BEGIN(ApiClient)

    API_CLIENT_INIT(DeviceClient)
    API_CALL("POST","DeviceUploadData/type2",uploadPost,BODY_DTO(Object<UploadDTO>,body))

    #include OATPP_CODEGEN_END(ApiClient)
};

#endif